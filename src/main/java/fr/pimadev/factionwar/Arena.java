package fr.pimadev.factionwar;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;

public class Arena {

    @Getter
    private Location loc1;
    @Getter
    private Location loc2;
    @Getter
    @Setter
    private boolean taken;

    public Arena(Location loc1, Location loc2) {
        this.loc1 = loc1;
        this.loc2 = loc2;
    }
}
