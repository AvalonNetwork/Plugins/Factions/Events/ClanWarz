package fr.pimadev.factionwar;

import com.massivecraft.factions.Faction;
import lombok.Getter;

public class Duel {

    private Main     main;
    @Getter
    private WFaction faction1;
    @Getter
    private WFaction faction2;
    @Getter
    private Arena    arena;

    public Duel(Main main, WFaction faction1, WFaction faction2, Arena arena) {
        this.main = main;
        this.faction1 = faction1;
        this.faction2 = faction2;
        this.arena = arena;
        this.faction1.teleport(arena.getLoc1());
        this.faction2.teleport(arena.getLoc2());
        this.faction1.sendStuff();
        this.faction2.sendStuff();
    }

    public WFaction[] getWFactions(Faction faction) {
        if (this.faction1.getFaction().getTag().equalsIgnoreCase(faction.getTag())) {
            return new WFaction[] {this.faction1, this.faction2};
        }
        else {
            return new WFaction[] {this.faction2, this.faction1};
        }
    }

}
