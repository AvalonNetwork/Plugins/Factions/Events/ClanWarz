package fr.pimadev.factionwar;

import com.massivecraft.factions.Faction;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

@AllArgsConstructor
public class EventsListener implements Listener {

    private Main main;

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        this.handleDeathOrQuit(e.getEntity(), e);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        this.handleDeathOrQuit(e.getPlayer(), null);
    }

    public void handleDeathOrQuit(Player p, PlayerDeathEvent e) {
        Faction faction = this.main.getPlayerFaction(p);
        if (p != null) {
            Duel duel = this.main.getDuel(faction);
            if (duel != null) {
                if (e != null) {
                    e.getDrops().clear();
                }
                WFaction[] wFactions = duel.getWFactions(faction);
                wFactions[0].getInDuelPlayers().remove(p);
                wFactions[0].getDeathPlayers().add(p);
                if (wFactions[0].getInDuelPlayers().size() < 1) {
                    wFactions[0].sendMessage(this.main.PREFIX_ERROR + "Vous avez perdu votre duel.");
                    wFactions[1].sendMessage(this.main.PREFIX + "Vous avez gagné le duel !");
                    wFactions[1].clearStuff();
                    wFactions[1].teleport(this.main.getSpawnLoc());
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), this.main.getCommandOnLose().replace("<faction_name>", wFactions[0].getFaction().getTag()));
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), this.main.getCommandOnWin().replace("<faction_name>", wFactions[1].getFaction().getTag()));
                    duel.getArena().setTaken(false);
                    this.main.getDuels().remove(duel);
                }
                else {
                    wFactions[0].sendMessage(this.main.PREFIX_ERROR + "§e§l" + p.getName() + " §cest mort.");
                }
            }
        }
    }

}
