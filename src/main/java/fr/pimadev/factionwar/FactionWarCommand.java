package fr.pimadev.factionwar;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import fr.pimadev.guiapi.GUIItem;
import fr.pimadev.guiapi.GUIWindow;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;

public class FactionWarCommand implements CommandExecutor {

    private final String CHOSE_PLAYER  = "§eCliquez pour §a§ll'ajouter";
    private final String REMOVE_PLAYER = "§eCliquez pour §c§lle supprimer";

    private Main main;

    public FactionWarCommand(Main main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(this.main.PREFIX_ERROR + "Seul un joueur peut executer cette commande.");
            return true;
        }
        Player p = (Player) sender;
        if (args.length != 2 || !args[0].equalsIgnoreCase("duel")) {
            this.sendHelp(p);
            return true;
        }
        Faction playerFaction = this.main.getPlayerFaction(p);
        if (playerFaction == null) {
            p.sendMessage(this.main.PREFIX_ERROR + "Vous n'avez pas de faction.");
            return true;
        }
        if (playerFaction.getFPlayerAdmin() == null || !playerFaction.getFPlayerAdmin().getName().equalsIgnoreCase(p.getName())) {
            p.sendMessage(this.main.PREFIX_ERROR + "Seul le chef de la faction peut effectuer cette action.");
            return true;
        }
        if (args[1].equalsIgnoreCase("accept")) {
            Faction invitationSender = this.main.getInvitationSender(playerFaction);
            if (invitationSender == null) {
                p.sendMessage(this.main.PREFIX_ERROR + "Vous n'avez pas ou plus de requête de duel en cours.");
                return true;
            }
            FPlayer invitationSenderAdmin = invitationSender.getFPlayerAdmin();
            if (invitationSenderAdmin == null || invitationSenderAdmin.getPlayer() == null) {
                p.sendMessage(this.main.PREFIX_ERROR + "Le chef de la faction §e§l" + invitationSender.getTag() + " §cn'est plus en ligne, le duel est annulé.");
                this.main.getInvitedFactions().remove(invitationSender.getTag());
                return true;
            }
            Invitation invitation = this.main.getInvitedFactions().get(invitationSender.getTag());
            this.main.getInvitedFactions().remove(invitationSender.getTag());
            WFaction  faction1 = new WFaction(invitationSender);
            WFaction  faction2 = new WFaction(playerFaction);
            GUIWindow gui1     = this.createChoosePlayerInventory(faction1, faction2, p, invitation.getParticipants());
            GUIWindow gui2     = this.createChoosePlayerInventory(faction2, faction1, invitationSenderAdmin.getPlayer(), invitation.getParticipants());
            gui1.show(p);
            gui2.show(invitationSenderAdmin.getPlayer());
            return true;
        }
        else if (args[1].equalsIgnoreCase("deny")) {
            Faction invitationSender = this.main.getInvitationSender(playerFaction);
            if (invitationSender == null) {
                p.sendMessage(this.main.PREFIX_ERROR + "Vous n'avez pas ou plus de requête de duel en cours.");
                return true;
            }
            invitationSender.sendMessage(this.main.PREFIX + "La faction §e§l" + playerFaction.getTag() + " §6vient de refuser le duel envoyé par votre faction.");
            playerFaction.sendMessage(this.main.PREFIX + "Votre faction vient de refuser le duel contre la faction §e§l" + invitationSender.getTag());
            this.main.getInvitedFactions().remove(invitationSender.getTag());
            return true;
        }
        else {
            Faction opponentFaction = Factions.getInstance().getByTag(args[1]);
            if (opponentFaction == null) {
                p.sendMessage(this.main.PREFIX_ERROR + "La faction §e§l" + args[1] + "§c n'existe pas.");
                return true;
            }
            if (this.main.isAlreadyInvited(opponentFaction.getTag())) {
                p.sendMessage(this.main.PREFIX_ERROR + "La faction §e§" + args[1] + "§c a déjà une requête de duel en cours. Elle doit d'abord l'accepter ou la refuser.");
                return true;
            }
            GUIWindow gui = this.createChooseNumberOfPlayersInDuelInventory(p.getName(), playerFaction, opponentFaction);
            gui.show(p);
            return true;
        }
    }

    private void sendHelp(CommandSender sender) {
        sender.sendMessage("§6/gf duel <nom_faction> : §eDemander un duel contre une faction");
        sender.sendMessage("§6/gf duel accept : §eAccepter le duel envoyé par une autre faction");
    }

    private GUIWindow createChooseNumberOfPlayersInDuelInventory(String playerName, Faction senderFaction, Faction opponentFaction) {
        GUIWindow gui = new GUIWindow("Nombre de joueurs dans le duel", 1, playerName);

        ItemStack sword1 = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta  meta1  = this.getItemMeta(sword1);
        meta1.setDisplayName("§6Duel avec §e§l3 joueurs §6par équipes");
        sword1.setItemMeta(meta1);

        ItemStack sword2 = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta  meta2  = this.getItemMeta(sword2);
        meta2.setDisplayName("§6Duel avec §e§l5 joueurs §6par équipes");
        sword2.setItemMeta(meta2);

        gui.setItem(this.main.SLOT_INDEX_3_PER_TEAMS, new GUIItem(sword1, (event) -> FactionWarCommand.this.sendInvitation(senderFaction, opponentFaction, 3)));
        gui.setItem(this.main.SLOT_INDEX_5_PER_TEAMS, new GUIItem(sword2, (event) -> FactionWarCommand.this.sendInvitation(senderFaction, opponentFaction, 5)));
        gui.setCloseEvent((event) -> gui.unregister());
        return gui;
    }

    public GUIWindow createChoosePlayerInventory(WFaction wFaction, WFaction opponentWFaction, Player sender, int maxPlayer) {
        GUIWindow gui = new GUIWindow("Choisir les joueurs pour le duel", 6, sender.getName());
        Integer   i   = 0;
        for (Player player : wFaction.getFaction().getOnlinePlayers()) {
            ItemStack skull     = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
            skullMeta.setOwner(player.getName());
            skullMeta.setDisplayName("§b" + player.getName());
            skullMeta.setLore(Arrays.asList(this.CHOSE_PLAYER));
            skull.setItemMeta(skullMeta);
            int index = i;
            gui.setItem(i, new GUIItem(skull, (event) -> {
                ItemStack stack = sender.getOpenInventory().getItem(index);
                SkullMeta meta  = (SkullMeta) stack.getItemMeta();
                if (meta.getLore().get(0).equalsIgnoreCase(this.REMOVE_PLAYER)) {
                    meta.setLore(Arrays.asList(this.CHOSE_PLAYER));
                    wFaction.getInDuelPlayers().remove(player);
                    sender.sendMessage(this.main.PREFIX + "Vous venez de §e§lsupprimer §6le joueur §e§l" + player.getName() + " §6du duel.");
                }
                else {
                    meta.setLore(Arrays.asList(this.REMOVE_PLAYER));
                    if (wFaction.getInDuelPlayers().size() >= maxPlayer) {
                        sender.sendMessage(this.main.PREFIX + "Vous avez atteint le nombre maximum de joueurs à ajouter au duel.");
                    }
                    else {
                        wFaction.getInDuelPlayers().add(player);
                        sender.sendMessage(this.main.PREFIX + "Vous venez §e§ld'ajouter §6le joueur §e§l" + player.getName() + " §6au duel.");
                    }
                }
                stack.setItemMeta(meta);
                sender.getOpenInventory().setItem(index, stack);
                sender.updateInventory();
            }));
            gui.setCloseEvent((event) -> {
                gui.unregister();
                wFaction.setReady(true);
                if (opponentWFaction.isReady()) {
                    Arena arena = this.main.getEmptyArena();
                    if (arena == null) {
                        wFaction.sendMessage(this.main.PREFIX_ERROR + "Il n'y à plus d'arene de libre, le duel ne peut pas avoir lieu");
                        opponentWFaction.sendMessage(this.main.PREFIX_ERROR + "Il n'y à plus d'arene de libre, le duel ne peut pas avoir lieu");
                    }
                    else {
                        arena.setTaken(true);
                        Duel duel = new Duel(this.main, wFaction, opponentWFaction, arena);
                        this.main.getDuels().add(duel);
                    }
                }
            });
            i++;
        }
        return gui;
    }

    public void sendInvitation(Faction sender, Faction opponentFaction, int participants) {
        sender.sendMessage(this.main.PREFIX + "Une demande de duel de faction vient d'être envoyé à la faction §e§l" + opponentFaction.getTag());
        opponentFaction.sendMessage(this.main.PREFIX + "Votre faction vient de recevoir une demande de duel par la faction §e§l" + sender.getTag());
        opponentFaction.sendMessage(this.main.PREFIX + "Le chef de la faction peut accepter ou refuser le duel avec la commande §e§l/gf duel accept/deny");
        this.main.getInvitedFactions().put(sender.getTag(), new Invitation(opponentFaction, participants));
    }

    private ItemMeta getItemMeta(ItemStack stack) {
        return stack.hasItemMeta() ? stack.getItemMeta() : Bukkit.getItemFactory().getItemMeta(stack.getType());
    }

}
