package fr.pimadev.factionwar;

import com.massivecraft.factions.Faction;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Invitation {

    @Getter
    private Faction invitedFaction;
    @Getter
    private int participants;

}
