package fr.pimadev.factionwar;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Main extends JavaPlugin {

    public final String      PREFIX                 = "";
    public final String      PREFIX_ERROR           = "";
    public final int         SLOT_INDEX_3_PER_TEAMS = 3;
    public final int         SLOT_INDEX_5_PER_TEAMS = 5;
    @Getter
    private      List<Arena> arenas                 = new ArrayList<>();
    @Getter
    private      List<Duel>  duels                  = new ArrayList<>();
    @Getter
    private Location spawnLoc;

    @Getter
    private String                  commandOnLose;
    @Getter
    private String                  commandOnWin;
    @Getter
    private Map<String, Invitation> invitedFactions = new HashMap<>();

    @Override
    public void onEnable() {
        super.onEnable();
        this.loadConfig();
        this.commandOnLose = this.getConfig().getString("command_on_lose");
        this.commandOnWin = this.getConfig().getString("command_on_win");
        int numberOfArenas = this.getConfig().getInt("arenas_number");
        for (int i = 1; i < numberOfArenas + 1; i++) {
            this.arenas.add(new Arena(this.getLoc(this.getConfig().getString("arena_" + i + "_pos1")), this.getLoc(this.getConfig().getString("arena_" + i + "_pos2"))));
        }
        this.spawnLoc = this.getLoc(this.getConfig().getString("spawn_location"));
        this.getCommand("gf").setExecutor(new FactionWarCommand(this));
        this.getServer().getPluginManager().registerEvents(new EventsListener(this), this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    private void loadConfig() {
        File config = new File(this.getConfig().getCurrentPath());
        if (!config.exists()) {
            this.getConfig().options().copyDefaults(true);
            this.saveConfig();
        }
    }

    public Location getLoc(String serializedLocation) {
        String[] str   = serializedLocation.split(",");
        String   world = str[0];
        double   x     = Double.valueOf(str[1]).doubleValue();
        double   y     = Double.valueOf(str[2]).doubleValue();
        double   z     = Double.valueOf(str[3]).doubleValue();
        return new Location(Bukkit.getWorld(world), x, y, z);
    }

    public boolean isAlreadyInvited(String factionName) {
        return this.invitedFactions.containsValue(factionName);
    }

    public Faction getInvitationSender(Faction faction) {
        for (Map.Entry<String, Invitation> entry : this.invitedFactions.entrySet()) {
            if (entry.getValue().getInvitedFaction() != null && entry.getValue().getInvitedFaction().getTag().equalsIgnoreCase(faction.getTag())) {
                return Factions.getInstance().getByTag(entry.getKey());
            }
        }
        return null;
    }

    public Arena getEmptyArena() {
        for (Arena arena : this.arenas) {
            if (!arena.isTaken()) {
                return arena;
            }
        }
        return null;
    }

    public Faction getPlayerFaction(Player p) {
        FPlayer fPlayer = FPlayers.getInstance().getByPlayer(p);
        if (fPlayer != null && fPlayer.hasFaction()) {
            return fPlayer.getFaction();
        }
        return null;
    }

    public Duel getDuel(Faction faction) {
        for(Duel duel : this.duels) {
            if(duel.getFaction1().getFaction().getTag().equalsIgnoreCase(faction.getTag()) || duel.getFaction2().getFaction().getTag().equalsIgnoreCase(faction.getTag())) {
                return duel;
            }
        }
        return null;
    }

}
