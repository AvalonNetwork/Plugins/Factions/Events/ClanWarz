package fr.pimadev.factionwar;

import com.massivecraft.factions.Faction;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class WFaction {

    @Getter
    private Faction      faction;
    @Getter
    private List<Player> inDuelPlayers = new ArrayList<>();
    @Getter
    private List<Player> deathPlayers = new ArrayList<>();
    @Getter
    @Setter
    private boolean      ready;

    public WFaction(Faction faction) {
        this.faction = faction;
    }

    public void sendMessage(String message) {
        for (Player p : this.inDuelPlayers) {
            p.sendMessage(message);
        }
        for (Player p : this.deathPlayers) {
            p.sendMessage(message);
        }
    }

    public void sendStuff() {
        for(Player player : this.inDuelPlayers) {
            player.getInventory().addItem(new ItemStack(Material.DIAMOND_SWORD));
        }
    }

    public void teleport(Location loc) {
        for(Player player : this.inDuelPlayers) {
            player.teleport(loc);
        }
    }

    public void clearStuff() {
        for(Player player : this.inDuelPlayers) {
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
        }
    }

}
